#Debian Stretch
deb http://ftp.at.debian.org/debian/ stretch main contrib non-free
deb-src http://ftp.at.debian.org/debian/ stretch main contrib non-free

#Debian Stretch Security Updates
deb http://security.debian.org/debian-security stretch/updates main contrib non-free
deb-src http://security.debian.org/debian-security stretch/updates main contrib non-free
